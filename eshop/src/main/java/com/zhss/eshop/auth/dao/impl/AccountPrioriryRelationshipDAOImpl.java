package com.zhss.eshop.auth.dao.impl;

import com.zhss.eshop.auth.dao.RolePrioriryRelationshipDAO;
import com.zhss.eshop.auth.mapper.AccountPrioriryRelationshipMapper;
import com.zhss.eshop.auth.mapper.RolePrioriryRelationshipMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @Description : 账号和权限关系管理模块DAO
 * @Author : BETTERME
 */
@Repository
public class AccountPrioriryRelationshipDAOImpl implements RolePrioriryRelationshipDAO {

    /**
     *账号和权限关系管理模块Mapper
     */
    @Autowired
    private AccountPrioriryRelationshipMapper accountPrioriryRelationshipMapper;

    /**
     * 根据权限id查询记录数
     * @param prioriryId 权限id
     * @return 记录数
     */
    @Override
    public Long getCountByPrioriryId(Long prioriryId) {
        return accountPrioriryRelationshipMapper.getCountByPrioriryId(prioriryId);
    }
}
