package com.zhss.eshop.auth.service;


import com.zhss.eshop.auth.domain.PriorityDO;
import com.zhss.eshop.auth.domain.PriorityDTO;

import java.util.List;

/**
 * @Description : 权限管理模块service组件
 * @Author : BETTERME
 */
public interface PriorityService {

    /**
     * 查询根权限
     * @return 根权限集合
     */
    List<PriorityDTO> listRootPriority();

    /**
     * 根据父权限id查询子权限
     * @param parentId 父权限id
     * @return 子权限
     */
    List<PriorityDTO> listChildPriorities(Long parentId );

    /**
     * 根据id查询权限
     * @param id 权限id
     * @return 权限
     */
    PriorityDTO getPriorityById( Long id );

    /**
     * 新增权限
     * @param priorityDTO 权限DO对象
     */
    Boolean savePriority(PriorityDTO priorityDTO);

    /**
     * 更新权限
     * @param priorityDTO 权限DO对象
     */
    Boolean updatePriority(PriorityDTO priorityDTO);

    /**
     * 删除权限
     * @param id 权限id
     * @return
     */
    Boolean removePriority (Long id);
}
