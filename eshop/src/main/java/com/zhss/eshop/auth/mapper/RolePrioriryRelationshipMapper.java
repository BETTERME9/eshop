package com.zhss.eshop.auth.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * @Description : 角色和权限关系管理模块Mapper
 * @Author : BETTERME
 */
@Mapper
public interface RolePrioriryRelationshipMapper {
    /**
     * 根据权限id查询记录数
     * @param prioriryId 权限id
     * @return 记录数
     */
    @Select("SELECT count(*) " +
            "FROM auth_role_priority_relationship " +
            "WHERE priority_id=#{prioriryId}")
    Long getCountByPrioriryId(@Param("prioriryId") Long prioriryId);
}
