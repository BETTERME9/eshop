package com.zhss.eshop.auth.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * @Description : 账号和权限关系管理模块的Mapper组件
 * @Author : BETTERME
 */
@Mapper
public interface AccountPrioriryRelationshipMapper {

    /**
     * 根据权限id查询记录数
     * @param prioriryId 权限id
     * @return 记录数
     */
    @Select("SELECT count(*) " +
            "FROM auth_account_priority_relationship " +
            "WHERE priority_id=#{prioriryId}")
    Long getCountByPrioriryId(@Param("prioriryId") Long prioriryId);
}
