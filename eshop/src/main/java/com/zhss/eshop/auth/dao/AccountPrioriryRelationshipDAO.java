package com.zhss.eshop.auth.dao;

/**
 * @Description : 账号和权限关系管理模块DAO
 * @Author : BETTERME
 */
public interface AccountPrioriryRelationshipDAO {

    /**
     * 根据权限id查询记录数
     * @param prioriryId 权限id
     * @return 记录数
     */
    Long getCountByPrioriryId(Long prioriryId);
}
