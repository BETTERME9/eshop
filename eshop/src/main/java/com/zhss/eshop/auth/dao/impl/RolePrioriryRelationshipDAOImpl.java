package com.zhss.eshop.auth.dao.impl;

import com.zhss.eshop.auth.dao.RolePrioriryRelationshipDAO;
import com.zhss.eshop.auth.mapper.RolePrioriryRelationshipMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @Description : 角色和权限关系管理模块DAO
 * @Author : BETTERME
 */
@Repository
public class RolePrioriryRelationshipDAOImpl implements RolePrioriryRelationshipDAO {

    /**
     *角色和权限关系管理模块Mapper
     */
    @Autowired
    private RolePrioriryRelationshipMapper rolePrioriryRelationshipMapper;

    /**
     * 根据权限id查询记录数
     * @param prioriryId 权限id
     * @return 记录数
     */
    @Override
    public Long getCountByPrioriryId(Long prioriryId) {
        return rolePrioriryRelationshipMapper.getCountByPrioriryId(prioriryId);
    }
}
