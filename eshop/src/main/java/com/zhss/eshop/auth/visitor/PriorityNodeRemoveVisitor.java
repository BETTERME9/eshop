package com.zhss.eshop.auth.visitor;

import com.zhss.eshop.auth.composite.PrioriryNode;
import com.zhss.eshop.auth.dao.PriorityDAO;
import com.zhss.eshop.auth.domain.PriorityDO;

import java.util.List;

/**
 * @Description : 权限树节点的删除访问者
 * @Author : BETTERME
 */
public class PriorityNodeRemoveVisitor implements PriorityNodeVisitor {

    /**
     * 权限管理模块DAO组件
     */
    private PriorityDAO priorityDAO;

    /**
     * 构造函数
     * @param priorityDAO 权限管理模块DAO
     */
    public PriorityNodeRemoveVisitor(PriorityDAO priorityDAO) {
        this.priorityDAO = priorityDAO;
    }

    /**
     * 访问权限树节点
     * @param node 权限树节点
     */
    @Override
    public void visit(PrioriryNode node) {

        List<PriorityDO> priorityDOS = priorityDAO.listChildPriorities(node.getId());

        /**
         * 递归
         */
        if (priorityDOS != null && priorityDOS.size() > 0){
            for (PriorityDO priorityDO : priorityDOS){
                PrioriryNode prioriryNode = priorityDO.clone(PrioriryNode.class);
                prioriryNode.accept(this);
            }
        }
        removePriority(node);
    }

    /**
     * 删除权限
     * @param node 节点
     */
    private void removePriority(PrioriryNode node){

        priorityDAO.removePriority(node.getId());
    }
}
