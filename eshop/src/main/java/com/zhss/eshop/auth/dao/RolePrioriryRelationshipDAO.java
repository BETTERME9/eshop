package com.zhss.eshop.auth.dao;

import org.apache.ibatis.annotations.Param;

/**
 * @Description : 角色和权限关系管理模块DAO
 * @Author : BETTERME
 */
public interface RolePrioriryRelationshipDAO {

    /**
     * 根据权限id查询记录数
     * @param prioriryId 权限id
     * @return 记录数
     */
    Long getCountByPrioriryId( Long prioriryId);
}
