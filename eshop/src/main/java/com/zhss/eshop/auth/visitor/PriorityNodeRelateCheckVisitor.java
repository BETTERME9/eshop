package com.zhss.eshop.auth.visitor;

import com.zhss.eshop.auth.composite.PrioriryNode;
import com.zhss.eshop.auth.dao.AccountPrioriryRelationshipDAO;
import com.zhss.eshop.auth.dao.PriorityDAO;
import com.zhss.eshop.auth.dao.RolePrioriryRelationshipDAO;
import com.zhss.eshop.auth.domain.PriorityDO;

import java.util.List;

/**
 * @Description : 权限树节点的关联检查访问者
 * @Author : BETTERME
 */
public class PriorityNodeRelateCheckVisitor implements PriorityNodeVisitor {

    /**
     * 关联检查结果
     */
    private Boolean relateCheckResult =false;

    /**
     * 权限管理模块DAO组件
     */
    private PriorityDAO priorityDAO;

    /**
     * 账号和权限关系管理模块DAO
     */
    private AccountPrioriryRelationshipDAO accountPrioriryRelationshipDAO;

    /**
     * 角色和权限管理模块DAO
     */
    private RolePrioriryRelationshipDAO rolePrioriryRelationshipDAO;

    /**
     * 构造函数
     * @param priorityDAO 权限管理模块的DAO组件
     * @param accountPrioriryRelationshipDAO 账号和权限关系管理模块DAO
     * @param rolePrioriryRelationshipDAO 角色和权限管理模块DAO
     */
    public PriorityNodeRelateCheckVisitor(PriorityDAO priorityDAO,
                                          RolePrioriryRelationshipDAO rolePrioriryRelationshipDAO,
                                          AccountPrioriryRelationshipDAO accountPrioriryRelationshipDAO){
        this.priorityDAO = priorityDAO;
        this.accountPrioriryRelationshipDAO = accountPrioriryRelationshipDAO;
        this.rolePrioriryRelationshipDAO = rolePrioriryRelationshipDAO;
    }

    /**
     * 访问权限树节点
     * @param node
     */
    @Override
    public void visit(PrioriryNode node) {
        List<PriorityDO> priorityDOS = priorityDAO.listChildPriorities(node.getId());

        /**
         * 递归
         */
        if (priorityDOS != null && priorityDOS.size() > 0){
            for (PriorityDO priorityDO : priorityDOS){
                PrioriryNode prioriryNode = priorityDO.clone(PrioriryNode.class);
                prioriryNode.accept(this);
            }
        }
        if(relateCheck(node)){
            this.relateCheckResult = true;
        }

    }

    /**
     * 检查权限是否被任何一个角色或者是账号关联了
     * @param node 权限树节点
     * @return 是否被任何一个角色或者是账号关联了，如果关联了返回true，如果没有关联则为false
     */
    private Boolean relateCheck(PrioriryNode node){
        Long roleRelateCount = rolePrioriryRelationshipDAO.getCountByPrioriryId(node.getId());
        if (roleRelateCount != null && roleRelateCount >0 ){
            return true;
        }

        Long accountRelateCount = accountPrioriryRelationshipDAO.getCountByPrioriryId(node.getId());
        if (accountRelateCount != null && accountRelateCount >0 ){
            return true;
        }

        return false;
    }

    public Boolean getRelateCheckResult() {
        return relateCheckResult;
    }
}
