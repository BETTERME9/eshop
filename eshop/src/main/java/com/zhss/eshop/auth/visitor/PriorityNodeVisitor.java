package com.zhss.eshop.auth.visitor;

import com.zhss.eshop.auth.composite.PrioriryNode;

/**
 * @Description : 权限树节点的访问者接口
 * @Author : BETTERME
 */
public interface PriorityNodeVisitor {

    void visit(PrioriryNode node);
}
